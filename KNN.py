import numpy as np
import knnClassifier
import preprocessing
import matplotlib.pyplot as plt
import KNNClassifier


def main():
    path = '/home/duyhv150601/source/vscode/learnCnns/dataset/cifar-10-batches-py/data_batch_1'
#     knn= knnClassifier.KnnClassifier()

    knn = KNNClassifier.KNNClassifier()
    pre = preprocessing.loadData(path=path)
    pre.unpickle()
    pre.split_data()
    X_train, X_test, Y_train, Y_test = pre.X_train, pre.X_test, pre.Y_train, pre.Y_test
    X_train = X_train[:800, :]
    Y_train = Y_train[:800]
    X_test, Y_test = X_test[:200, :], Y_test[:200]

    knn.train(X=X_train, Y=Y_train)
    print(X_train.shape, np.shape(X_test))
    print(np.shape(knn.Y_train), np.shape(Y_test))
    knn.train(X_train, Y_train)
    # 0 loop is faster than 1 and 2 loop very much
    dicts = knn.l2Norm2Loop(X_test)
    print(dicts.shape)
#     plt.imshow(dicts, interpolation= 'none')
#     plt.show()
    y_test_pred = knn.predictLabel(dicts, k=1)
    num_correct = np.sum(y_test_pred == Y_test)
    num_test = np.shape(Y_test)[0]
    accuracy = float(num_correct) / num_test
    print('Got %d/ %d correct => accuracy %f' %
          (num_correct, num_test, accuracy))

    numFolds = 5
    k_choices = [1, 3, 5, 8, 10, 12, 15, 20, 50, 100]
    X_trainFold = []
    Y_trainFold = []
    Y_train = np.reshape(Y_train, (-1, 1))
    X_trainFold, Y_trainFold = np.split(X_train, 5), np.split(Y_train, 5)
    kToAccuracies = {}

    for k_ in k_choices:
        kToAccuracies.setdefault(k_, [])
    print(kToAccuracies)

    for i in range(numFolds):
        classifier = KNNClassifier.KNNClassifier()
        XValTrain = np.vstack(X_trainFold[0: i] + X_trainFold[i + 1:])
        YValTrain = np.vstack(Y_trainFold[0: i] + Y_trainFold[i + 1:])
        YValTrain = YValTrain[:, 0]
        classifier.train(XValTrain, YValTrain)
        for k in k_choices:
            YValPred = classifier.predict(X_trainFold[i], k)
            num_correct = np.sum(YValPred == Y_trainFold[i][:, 0])
            accuracy = float(num_correct)/len(YValPred)
            kToAccuracies[k_] = kToAccuracies[k_] + [accuracy]

    for i in sorted(kToAccuracies):
            for accuracy in kToAccuracies[i]:
                    print('k= %d, accuracy= %f'%(i, accuracy))
    pass


if __name__ == '__main__':
    main()
