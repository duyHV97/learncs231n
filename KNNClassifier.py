import preprocessing as pr
import numpy as np


class KNNClassifier(object):
    def __init__(self):
        pass

    """
    a KNN classifier with L2 distance
    """
    # step 1: Just remember all training data set

    def train(self, X, Y):
        self.X_train = X
        self.Y_train = Y
        
    

    def l2Norm2Loop(self, X_test):
        """
        Input: 
            X_test: (numTest, D)
        Return:
            dists: (numTest, numTrain), where dists[i,j] is the Euclidean distance between the testPoint ith and the trainingPoint jth
        """
        numTest = np.shape(X_test)[0]
        numTrain = np.shape(self.X_train)[0]

        dists = np.ones((numTest, numTrain))

        for i in range(numTest):
            for j in range(numTrain):
                # l2norm function between X_train[j] and X_test[i]
                dists[i, j] = np.linalg.norm(self.X_train[j, :] - X_test[i, :])

        return dists

    def l2Norm1Loop(self, X_test):
        """
        Input: 
            X_test: (numTest, D)
        Output:
            dists: (numTest, numTrain): where dists[i, j] is the l2norm(X_test[i, :]- X_train[j: ])
        """
        numTest = np.shape(X_test)[0]
        numTrain = np.shape(self.X_train)[0]
        dists = np.zeros((numTest, numTrain))
        for i in range(numTest):
            dists[i, :] = np.linalg.norm(self.X_train - X_test[i, :])

        return dists

    def l2Norm0Loop(self, X_test):
        num_test = np.shape(X_test)[0]
        num_train = np.shape(self.X_train)[0]
        dists = np.zeros((num_test, num_train))

        # dists= sqrt(sum(squared(X_test- Xtrain)) <=> sqrt( X_test^2.T + X_train^2 -2*X_train.T.dot(X_test))

        M= X_test.dot(self.X_train.T)
        # print(M.shape)
        Xtr= np.square(self.X_train).sum(axis= 1)
        # print(Xtr.shape)
        Xte= np.square(X_test).sum(axis= 1)
        # print(Xte.shape)
        dists= np.sqrt(Xtr-2*M+ np.matrix(Xte).T) 
        # print(dists.shape)
        return dists

    def predictLabel(self, dists, k= 1):
        """
        Input: 
            dists: (numtest, numTrain)
        Output:
            Y: (numTest, ) the predict label for the X_train X[i]
        """

        numTest= dists.shape[0]
        yPredic= np.zeros(numTest)
        for i in range(numTest):
            closestY= []
            Y= np.array(self.Y_train)
            labels= Y[np.argsort(dists[i, :])] # np.argsort() sort the matrix by increment value of each element and return the matrix of index
            
            closestY= labels[:k]
            closestY= closestY.flatten()
            countNum= np.bincount(closestY)
            maxIndex= np.argmax(countNum)
            yPredic[i]= maxIndex
        return yPredic

    def predict(self, X_test, k, numLoop= 0):
        if numLoop== 0:
            dists= self.l2Norm0Loop(X_test)
        # if numLoop== 1:
        #     dists= self.l2Norm1Loop(X_test)
        # if numLoop== 2:
        #     dists= self.l2Norm2Loop(X_test)

        else:
            raise ValueError('Invalid value %d for numLoop'%numLoop)
        return self.predictLabel(dists, k= k)
