import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, KFold
import matplotlib.pyplot as plt
import pickle


class loadData:

    def __init__(self, path):
        self.path = path

    def unpickle(self):
        with open(self.path, 'rb') as fo:
            self.dic = pickle.load(fo, encoding='bytes')
            self.X = self.dic[b'data']
            self.Y = self.dic[b'labels']
            fo.close()
        # return X, Y
        return self

    def split_data(self):
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(
            self.X, self.Y, test_size=0.2)
        # return X_train, X_test, Y_train, Y_test
        return self

    # def slpit_data_into_Kfold(self, n):
    #     self.kf = KFold(n_splits=n)
    #     self
# X, Y= unpickle(path)
# X_train, X_test, Y_train, Y_test= split_data(X, Y)
# def main():
#     path= '/home/duyhv150601/Documents/cs231n/assignment/assignment1/cs231n/datasets/cifar-10-batches-py/data_batch_1'

#     load_Data = loadData(path)

#     load_Data.unpickle()
#     load_Data.split_data()
# X, Y= load_Data.X, load_Data.Y
# loadData.unpickle(path)
# X_train, X_test, Y_train, Y_test= split_data(X, Y)
    # load_Data.slpit_data_into_Kfold(5)
    # for train_ind, test_ind in load_Data.kf.split(load_Data.X_train):
    #     print("Train:{} ".format(str(np.shape(train_ind))), train_ind, "Test:{} ".format(str(np.shape(test_ind))), test_ind)
    # print(np.shape(load_Data.X_train))
    # print(np.shape(load_Data.Y_train))
    # classes = ['plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
    # num_classes = len(classes)
    # samples_per_class = 7
    # for y, clas in enumerate(classes):
    #     idxs= np.flatnonzero(load_Data.Y_train== y)
    #     idxs= np.random.choice(idxs, samples_per_class, replace= False)
    #     for i, idx in enumerate(idxs):
    #         plt_idx= i*num_classes + y+ 1
    #         plt.subplot(samples_per_class, num_classes, plt_idx)
    #         plt.imshow(load_Data.X_train[idx].astype('uint8'))
    #         plt.axis('off')
    #         if i == 0:
    #             plt.title(clas)
    # plt.show()
        # print(load_Data.Y)
# if __name__ == '__main__':
#     main()
